#Class Error
class Error:
    error = str

    def __init__(self, error):
        self.error = error

#Abstract class responce of database provider
class Responce:
    Data = None
    Error = None

    def __init__(self, T):
        if isinstance(T, Error):
            self.Error = T
        else:
            self.Data = T
    
    def error(self) -> str:
        return self.Error.error