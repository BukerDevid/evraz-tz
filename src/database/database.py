#Модуль взаимодействия с БД
from sqlalchemy.pool import QueuePool
from models.responce import Responce, Error
from models.note import Note
from models.statistics import StatisticNote
from hashlib import sha256
import time
import sqlalchemy

# def singleton(class_):
#     instances = {}
#     def getinstance(*args, **kwargs):
#         if class_ not in instances:
#             instances[class_] = class_(*args, **kwargs)
#         return instances[class_]
#     return getinstance

# @singleton
class Database:
    engine = sqlalchemy.engine
    Connect = sqlalchemy.engine.Connection
    Pool = QueuePool

    def __init__(self):
        try:
            self.engine = sqlalchemy.create_engine('sqlite:///notes.db', pool_size=5, max_overflow=1, poolclass=QueuePool)
            self.Pool = QueuePool(creator=self.engine.connect)
        except:
            print('[PANIC] init database')
            exit(1)
        print('[SUCCESS] init database')

    def DatabaseINFO(self):
        print('[INFO] SQLAlchimy - ', sqlalchemy.__version__)
        print('[INFO] SQLite 3')
    
    # TakeConnect - function take a connection from sqlalchemy engine 
    def TakeConnect(self):
        return self.engine.connect

    # Check_database - fonction for check tables in database
    def Check_database(self):
        self.DatabaseINFO()
        # Check users table
        try:
            connect = self.TakeConnect()
            connect().execute('create table if not exists users (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, email TEXT NOT NULL, pwd TEXT NOT NULL, firstname TEXT NOT NULL, lastname TEXT, middlename TEXT, UNIQUE(email));')
        except:
            print('[ERROR] check database. Check users table')
            exit(1)

        # Check status_note table
        try:
            connect = self.TakeConnect()
            connect().execute('create table if not exists status_note (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, name TEXT NOT NULL, UNIQUE(name));')
        except:
            print('[ERROR] check database. Check status_note table')
            exit(1)

        # Check task_note
        try:
            connect = self.TakeConnect()
            connect().execute('create table if not exists task_note (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, user INTEGER, date_create INTEGER NOT NULL, date_start INTEGER, date_update INTEGER, title TEXT NOT NULL, src TEXT NOT NULL, stat INTEGER NOT NULL, FOREIGN KEY (stat) REFERENCES status_note(id));')
        except:
            print('[ERROR] check database. Check task_note table')
            exit(1)

        # Check token_session
        try:
            connect = self.TakeConnect()
            connect().execute('create table if not exists token_session (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, user INTEGER NOT NULL, token TEXT NOT NULL, date_update INTEGER NOT NULL, FOREIGN KEY (user) REFERENCES users (id));')
        except:
            print('[ERROR] check database. Check token_session table')
            exit(1)
        
        print('[SUCCESS] check all tables database')

    # DefaultDataSet - function for set default data in satabase
    def DefaultDataSet(self):
        # Add default user
        try:
            connect = self.TakeConnect()
            res = connect().execute("select count(*) as 'all' from users;")
            res = res.fetchone()
            if res['all'] == 0:
                connect().execute("insert into users(email,pwd,firstname,lastname,middlename) values ('admin@mail.ru','8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918','Иван','Иванов','Иванович');")
            else:
                print('[INFO] In your database exists a users.')
        except:
            print('[ERROR] add default. Check users table')
            exit(1)

        # Add defaults status note
        try:
            connect = self.TakeConnect()
            res = connect().execute("select count(*) as 'all' from status_note;")
            res = res.fetchone()
            if res['all'] == 0:
                connect().execute("insert into status_note(name) values ('Без исполнителя'), ('Исполняется'), ('Отмена'), ('Завершено');")
            else:
                print('[INFO] In your database exists a statuses of note.')
        except:
            print('[ERROR] add default. Check status_note table')
            exit(1)
        print('[SUCCESS] add defaults data in database')

    # SelectCheckLoginAndPassword - function for check email and password to a user
    def SelectCheckLoginAndPassword(self, email, pwd) -> Responce:
        try:
            connect = self.TakeConnect()
            password = sha256(pwd.encode('utf-8')).hexdigest()
            print(password)
            res = connect().execute("select id from users where email = '{email}' AND pwd = '{pwd}';".format(email=email,pwd=password))
            res = res.fetchone()
            if res['id'] is None:
                return Responce(Error('User does not exist'))
            else:
                token = email+str((time.time()))
                token = sha256(token.encode('utf-8')).hexdigest()
                try:
                    connect = self.TakeConnect()
                    connect().execute('delete from token_session WHERE id = {id}'.format(id=res['id']))
                    connect().execute("insert into token_session(user,token,date_update) values ('{id}','{token}',{date_update})".format(id=res['id'],token=token,date_update=int(time.time())))
                except:
                    return Responce(Error('Filed create token'))
                return Responce(token)
        except:
            return Responce(Error('Unknow database error'))

    # SelectDeleteToken - function for delete token of session
    def SelectDeleteToken(self, token) -> Responce:
        try:
            connect = self.TakeConnect()
            connect().execute("delete from token_session WHERE token = '{token}';".format(token=token))
        except:
            return Responce(Error('Unknow database error'))
        return Responce('Success')

    # SelectCheckToken - function for delete token of session
    def SelectCheckToken(self, token) -> Responce:
        try:
            connect = self.TakeConnect()
            res = connect().execute("select user as user from token_session WHERE token = '{token}';".format(token=token))
            res = res.fetchone()
            if res['user'] is None:
                return Responce(Error('Token doesn not exist'))
            return Responce(res['user'])
            # return Responce(1)
        except:
            return Responce(Error('Unknow database error'))

    # SelectCreateNote - function for create note in database
    def SelectCreateNote(self, title, value) -> Responce:
        try:
            connect = self.TakeConnect()
            time_create = int(time.time())
            connect().execute("insert into task_note(date_create,date_start,date_update,title,src,stat) values ({date_create},0,{date_update},'{title}','{src}',1);".format(
                date_create=time_create, date_update=time_create, title=title, src=value 
            ))
            return Responce('Success!')
        except:
            return Responce(Error('Unknow database error'))

    # SelectUpdateStatus - function for update status note
    def SelectUpdateStatus(self, note_id, status) -> Responce:
        try:
            connect = self.TakeConnect()
            connect().execute("update task_note set stat = {status}, date_update = {date} where id = {id}".format(
                id=note_id, status=status, date=int(time.time())
            ))
            return Responce('Success!')
        except:
            return Responce(Error('Unknow database error'))

    # SelectNotes - function for request notes by filter
    def SelectNotes(self, filter, user_id=None) -> Responce:
        listNote = list()
        try:
            res = list()
            if filter == 'new':
                # SELECT 
                # task_note.id AS id, 
                # CASE WHEN task_note.user IS NULL THEN 'Пользователь не определен' ELSE (SELECT firstname FROM users WHERE users.id = task_note.user) END AS user, 
                # strftime('%H:%M:%S',task_note.date_create,'unixepoch','+3 hours') AS date_create, 
                # CASE WHEN task_note.date_start = 0 THEN 'Ожидает начала' ELSE strftime('%H:%M:%S',task_note.date_start,'unixepoch','+3 hours') END  AS date_start, 
                # strftime('%H:%M:%S',task_note.date_update,'unixepoch','+3 hours') AS date_update, 
                # task_note.title AS title, task_note.src AS note, status_note.name AS status 
                # FROM task_note JOIN status_note ON status_note.id = task_note.stat;
                connect = self.TakeConnect()
                res = connect().execute("SELECT task_note.id, CASE WHEN task_note.user IS NULL THEN 'Пользователь не определен' ELSE (SELECT firstname FROM users WHERE users.id = task_note.user) END AS user, strftime('%H:%M:%S',task_note.date_create,'unixepoch','+3 hours') AS date_create, CASE WHEN task_note.date_start = 0 THEN 'Ожидает начала' ELSE strftime('%H:%M:%S',task_note.date_start,'unixepoch','+3 hours') END  AS date_start, strftime('%H:%M:%S',task_note.date_update,'unixepoch','+3 hours') AS date_update, task_note.title AS title, task_note.src AS note, status_note.name AS status FROM task_note JOIN status_note ON status_note.id = task_note.stat WHERE task_note.stat = 1;")
            elif filter == 'user':
                # SELECT 
                # task_note.id AS id, 
                # CASE WHEN task_note.user IS NULL THEN 'Пользователь не определен' ELSE (SELECT firstname FROM users WHERE users.id = task_note.user) END AS user, 
                # strftime('%H:%M:%S',task_note.date_create,'unixepoch','+3 hours') AS date_create, 
                # CASE WHEN task_note.date_start = 0 THEN 'Ожидает начала' ELSE strftime('%H:%M:%S',task_note.date_start,'unixepoch','+3 hours') END  AS date_start, 
                # strftime('%H:%M:%S',task_note.date_update,'unixepoch','+3 hours') AS date_update, 
                # task_note.title AS title, task_note.src AS note, status_note.name AS status 
                # FROM task_note JOIN status_note ON status_note.id = task_note.stat WHERE task_note.user = {id};
                connect = self.TakeConnect()
                res = connect().execute("SELECT task_note.id, CASE WHEN task_note.user IS NULL THEN 'Пользователь не определен' ELSE (SELECT firstname FROM users WHERE users.id = task_note.user) END AS user, strftime('%H:%M:%S',task_note.date_create,'unixepoch','+3 hours') AS date_create, CASE WHEN task_note.date_start = 0 THEN 'Ожидает начала' ELSE strftime('%H:%M:%S',task_note.date_start,'unixepoch','+3 hours') END  AS date_start, strftime('%H:%M:%S',task_note.date_update,'unixepoch','+3 hours') AS date_update, task_note.title AS title, task_note.src AS note, status_note.name AS status FROM task_note JOIN status_note ON status_note.id = task_note.stat  WHERE task_note.user = {user_id} AND task_note.stat = 2;".format(user_id=user_id))
            elif filter == 'inProcess':
                # SELECT 
                # task_note.id AS id, 
                # CASE WHEN task_note.user IS NULL THEN 'Пользователь не определен' ELSE (SELECT firstname FROM users WHERE users.id = task_note.user) END AS user, 
                # strftime('%H:%M:%S',task_note.date_create,'unixepoch','+3 hours') AS date_create, 
                # CASE WHEN task_note.date_start = 0 THEN 'Ожидает начала' ELSE strftime('%H:%M:%S',task_note.date_start,'unixepoch','+3 hours') END  AS date_start, 
                # strftime('%H:%M:%S',task_note.date_update,'unixepoch','+3 hours') AS date_update, 
                # task_note.title AS title, task_note.src AS note, status_note.name AS status 
                # FROM task_note JOIN status_note ON status_note.id = task_note.stat WHERE task_note.stat = 2; 
                connect = self.TakeConnect()
                res = connect().execute("SELECT task_note.id, CASE WHEN task_note.user IS NULL THEN 'Пользователь не определен' ELSE (SELECT firstname FROM users WHERE users.id = task_note.user) END AS user, strftime('%H:%M:%S',task_note.date_create,'unixepoch','+3 hours') AS date_create, CASE WHEN task_note.date_start = 0 THEN 'Ожидает начала' ELSE strftime('%H:%M:%S',task_note.date_start,'unixepoch','+3 hours') END  AS date_start, strftime('%H:%M:%S',task_note.date_update,'unixepoch','+3 hours') AS date_update, task_note.title AS title, task_note.src AS note, status_note.name AS status FROM task_note JOIN status_note ON status_note.id = task_note.stat WHERE task_note.stat = 2;")
            elif filter == 'isReady':
                # SELECT 
                # task_note.id AS id, 
                # CASE WHEN task_note.user IS NULL THEN 'Пользователь не определен' ELSE (SELECT firstname FROM users WHERE users.id = task_note.user) END AS user, 
                # strftime('%H:%M:%S',task_note.date_create,'unixepoch','+3 hours') AS date_create, 
                # CASE WHEN task_note.date_start = 0 THEN 'Ожидает начала' ELSE strftime('%H:%M:%S',task_note.date_start,'unixepoch','+3 hours') END  AS date_start, 
                # strftime('%H:%M:%S',task_note.date_update,'unixepoch','+3 hours') AS date_update, 
                # task_note.title AS title, task_note.src AS note, status_note.name AS status 
                # FROM task_note JOIN status_note ON status_note.id = task_note.stat WHERE task_note.stat = 4;
                connect = self.TakeConnect()
                res = connect().execute("SELECT task_note.id, CASE WHEN task_note.user IS NULL THEN 'Пользователь не определен' ELSE (SELECT firstname FROM users WHERE users.id = task_note.user) END AS user, strftime('%H:%M:%S',task_note.date_create,'unixepoch','+3 hours') AS date_create, CASE WHEN task_note.date_start = 0 THEN 'Ожидает начала' ELSE strftime('%H:%M:%S',task_note.date_start,'unixepoch','+3 hours') END  AS date_start, strftime('%H:%M:%S',task_note.date_update,'unixepoch','+3 hours') AS date_update, task_note.title AS title, task_note.src AS note, status_note.name AS status FROM task_note JOIN status_note ON status_note.id = task_note.stat WHERE task_note.stat = 4;")
            elif filter == 'isCancel':
                # SELECT 
                # task_note.id AS id, 
                # CASE WHEN task_note.user IS NULL THEN 'Пользователь не определен' ELSE (SELECT firstname FROM users WHERE users.id = task_note.user) END AS user, 
                # strftime('%H:%M:%S',task_note.date_create,'unixepoch','+3 hours') AS date_create, 
                # CASE WHEN task_note.date_start = 0 THEN 'Ожидает начала' ELSE strftime('%H:%M:%S',task_note.date_start,'unixepoch','+3 hours') END  AS date_start, 
                # strftime('%H:%M:%S',task_note.date_update,'unixepoch','+3 hours') AS date_update, 
                # task_note.title AS title, task_note.src AS note, status_note.name AS status 
                # FROM task_note JOIN status_note ON status_note.id = task_note.stat WHERE task_note.stat = 3;
                connect = self.TakeConnect()
                res = connect().execute("SELECT task_note.id, CASE WHEN task_note.user IS NULL THEN 'Пользователь не определен' ELSE (SELECT firstname FROM users WHERE users.id = task_note.user) END AS user, strftime('%H:%M:%S',task_note.date_create,'unixepoch','+3 hours') AS date_create, CASE WHEN task_note.date_start = 0 THEN 'Ожидает начала' ELSE strftime('%H:%M:%S',task_note.date_start,'unixepoch','+3 hours') END  AS date_start, strftime('%H:%M:%S',task_note.date_update,'unixepoch','+3 hours') AS date_update, task_note.title AS title, task_note.src AS note, status_note.name AS status FROM task_note JOIN status_note ON status_note.id = task_note.stat WHERE task_note.stat = 3;")
            elif filter == 'archive':
                # SELECT 
                # task_note.id AS id, 
                # CASE WHEN task_note.user IS NULL THEN 'Пользователь не определен' ELSE (SELECT firstname FROM users WHERE users.id = task_note.user) END AS user, 
                # strftime('%H:%M:%S',task_note.date_create,'unixepoch','+3 hours') AS date_create, 
                # CASE WHEN task_note.date_start = 0 THEN 'Ожидает начала' ELSE strftime('%H:%M:%S',task_note.date_start,'unixepoch','+3 hours') END  AS date_start, 
                # strftime('%H:%M:%S',task_note.date_update,'unixepoch','+3 hours') AS date_update, 
                # task_note.title AS title, task_note.src AS note, status_note.name AS status 
                # FROM task_note JOIN status_note ON status_note.id = task_note.stat WHERE task_note.stat = 3 AND take_note.stat = 4;
                connect = self.TakeConnect()
                res = connect().execute("SELECT task_note.id, CASE WHEN task_note.user IS NULL THEN 'Пользователь не определен' ELSE (SELECT firstname FROM users WHERE users.id = task_note.user) END AS user, strftime('%H:%M:%S',task_note.date_create,'unixepoch','+3 hours') AS date_create, CASE WHEN task_note.date_start = 0 THEN 'Ожидает начала' ELSE strftime('%H:%M:%S',task_note.date_start,'unixepoch','+3 hours') END  AS date_start, strftime('%H:%M:%S',task_note.date_update,'unixepoch','+3 hours') AS date_update, task_note.title AS title, task_note.src AS note, status_note.name AS status FROM task_note JOIN status_note ON status_note.id = task_note.stat WHERE task_note.stat = 3 OR task_note.stat = 4;")
            else:
                connect = self.TakeConnect()
                res = connect().execute("SELECT task_note.id, CASE WHEN task_note.user IS NULL THEN 'Пользователь не определен' ELSE (SELECT firstname FROM users WHERE users.id = task_note.user) END AS user, strftime('%H:%M:%S',task_note.date_create,'unixepoch','+3 hours') AS date_create, CASE WHEN task_note.date_start = 0 THEN 'Ожидает начала' ELSE strftime('%H:%M:%S',task_note.date_start,'unixepoch','+3 hours') END  AS date_start, strftime('%H:%M:%S',task_note.date_update,'unixepoch','+3 hours') AS date_update, task_note.title AS title, task_note.src AS note, status_note.name AS status FROM task_note JOIN status_note ON status_note.id = task_note.stat")
            # if len(res) == 0:
            #     return Responce(Error('Empty list'))
            for row in res:
                print(row.id)
                listNote.append(Note(
                    ID=row.id,
                    User=row.user,
                    DateCreate=row.date_create,
                    DateStart=row.date_start,
                    DateUpdate=row.date_update,
                    Title=row.title,
                    Value=row.note,
                    Status=row.status
                ))
                    
            return Responce(listNote)
        except:
            return Responce(Error('Unknow database error'))

    def SelectChoosUserOnNote(self, note_id, user_id) -> Responce:
        try:
            connect = self.TakeConnect()
            connect().execute("update task_note set stat = 2, user = {user_id}, date_start = {date_start}, date_update = {date_start} where id = {note_id}".format(
                note_id=note_id, user_id=user_id, date_start=time.time()
            ))
            return Responce('Success!')
        except:
            return Responce(Error('Unknow database error'))
    
    def SelectStatistics(self) -> Responce:
        try:
            connect = self.TakeConnect()
            res = connect().execute("SELECT * FROM (SELECT count(*) FROM task_note WHERE stat = 1) AS new, (SELECT count(*) FROM task_note WHERE stat = 2) AS active, (SELECT count(*) FROM task_note WHERE stat = 4) AS ready, (SELECT count(*) FROM task_note WHERE stat = 3) AS cancel;")
            res = res.fetchone()
            # All notes
            StatisticNote.NewNote = res[0]
            StatisticNote.ActiveNote = res[1]
            StatisticNote.ReadyNote = res[2]
            StatisticNote.CancelNote = res[3]
            # Average time
            res = connect().execute("SELECT date_update-date_start AS time FROM task_note WHERE stat = 4;")
            middleTime = 0
            for row in res:
                middleTime = middleTime + row.time
            if middleTime != 0:
                middleTime = middleTime / StatisticNote.ReadyNote
            StatisticNote.TimingWork = time.strftime('%H:%M:%S', time.gmtime(middleTime))
            print(StatisticNote.NewNote)
            return Responce(StatisticNote)
        except:
            return Responce(Error('Unknow database error'))