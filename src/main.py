from os import error
import sys
sys.path.append('./database')
from database.database import Database
from flask import Flask, request, render_template, redirect
# _____________________ #
HOST='localhost:5000'
# _____________________ #
app = Flask(__name__)

print('Start server TASK-NOTE')
base = Database()
base.Check_database()
base.DefaultDataSet()

@app.route('/')
def index():
    db = Database()
    token = request.cookies.get('session')
    print('token -', token)
    data = db.SelectCheckToken(token)
    if data.Error:
        return redirect("http://{host}/login".format(host=HOST), code=302)
    else:
        return redirect("http://{host}/home".format(host=HOST), code=302) 

@app.route('/login', methods=['GET','POST'])
def login():
    db = Database()
    token = request.cookies.get('session')
    data = db.SelectCheckToken(token)
    if data.Error:
        if request.method == 'GET':
            return render_template('login.html')
        elif request.method == 'POST':
            data = db.SelectCheckLoginAndPassword(request.form['email'], request.form['pwd'])
            if data.Error:
                return render_template('login.html', error=data.Error.error)
            else:
                resp = redirect("http://{host}/home".format(host=HOST), code=302)
                resp.set_cookie('session',data.Data)
                return resp
    else:
        return redirect("http://{host}/home".format(host=HOST), code=302)

@app.route('/home', methods=['GET','POST'])
def home():
    db = Database()
    token = request.cookies.get('session')
    user = db.SelectCheckToken(token)
    if request.method == 'GET':
        if user.Error:
            return redirect("http://{host}/login".format(host=HOST), code=302)
        else:
            notes = db.SelectNotes('new')
            if notes.Error:
                return render_template('note.html',host=HOST,empty='Произошла ошибка. Попробуйте обновить страницу!')
            else:
                if len(notes.Data) == 0:
                    return render_template('note.html',host=HOST,empty='Новые задачи отсутствую')
                else:
                    return render_template('note.html',host=HOST,notes=notes)
    if request.method == 'POST':
        if user.Error:
            return redirect("http://{host}/login".format(host=HOST), code=302)
        else:
            _ = db.SelectChoosUserOnNote(request.form['note'],user.Data)
            notes = db.SelectNotes('new')
            if notes.Error:
                return render_template('note.html',host=HOST,empty='Произошла ошибка. Попробуйте обновить страницу!')
            else:
                if len(notes.Data) == 0:
                    return render_template('note.html',host=HOST,empty='Задачи закончились, ожидайте поступления новых!')
                else:
                    return render_template('note.html',host=HOST,notes=notes)

@app.route('/add-note', methods=['GET','POST'])
def add_note():
    db = Database()
    token = request.cookies.get('session')
    data = db.SelectCheckToken(token)
    if request.method == 'GET':
        if data.Error:
            return render_template('add_note.html',host=HOST)
        else:
            return render_template('add_note.html',user=True,host=HOST)
    elif request.method == 'POST':
        dataCreateNote = db.SelectCreateNote(request.form['title'], request.form['value'])
        if data.Error:
            if dataCreateNote.Error:
                return render_template('add_note.html',host=HOST,error='Не получилось отправить запрос.')
            else:
                return render_template('add_note.html',host=HOST,success='Запрос успешно отправлен!')
        else:
            if dataCreateNote.Error:
                return render_template('add_note.html',user=True,host=HOST,error='Не получилось отправить запрос.')
            else:
                return render_template('add_note.html',user=True,host=HOST,success='Запрос успешно отправлен!')

@app.route('/my-note',methods=['GET'])
def my_note():
    db = Database()
    token = request.cookies.get('session')
    user = db.SelectCheckToken(token)
    if request.method == 'GET':
        if user.Error:
            return redirect("http://{host}/login".format(host=HOST), code=302)
        else:
            notes = db.SelectNotes('user', user_id=user.Data)
            if notes.Error:
                return render_template('my_note.html',host=HOST,empty='Произошла ошибка. Попробуйте обновить страницу!')
            else:
                if len(notes.Data) == 0:
                    return render_template('my_note.html',host=HOST,empty='Список пуст, выбирите задачу!')
                else:
                    return render_template('my_note.html',host=HOST,notes=notes)

@app.route('/set-note-ready',methods=['POST'])
def set_note_ready():
    db = Database()
    token = request.cookies.get('session')
    user = db.SelectCheckToken(token)
    if user.Error:
        return redirect("http://{host}/login".format(host=HOST), code=302)
    else:
        setNoteStatReady = db.SelectUpdateStatus(request.form['note'],4)
        if setNoteStatReady.Error:
            notes = db.SelectNotes('user', user_id=user.Data)
            if notes.Error:
                return render_template('my_note.html',host=HOST,empty='Произошла ошибка. Попробуйте обновить страницу!')
            else:
                if len(notes.Data) == 0:
                    return render_template('my_note.html',host=HOST,empty='Список пуст, выбирите задачу!')
                else:
                    return render_template('my_note.html',host=HOST,notes=notes,note=int(request.form['note']),method="создать")
        else:
            return redirect("http://{host}/my-note".format(host=HOST), code=302)


@app.route('/set-note-cancel',methods=['POST'])
def set_note_cancel():
    db = Database()
    token = request.cookies.get('session')
    user = db.SelectCheckToken(token)
    if user.Error:
        return redirect("http://{host}/login".format(host=HOST), code=302)
    else:
        setNoteStatReady = db.SelectUpdateStatus(request.form['note'],3)
        if setNoteStatReady.Error:
            notes = db.SelectNotes('user', user_id=user.Data)
            if notes.Error:
                return render_template('my_note.html',host=HOST,empty='Произошла ошибка. Попробуйте обновить страницу!')
            else:
                if len(notes.Data) == 0:
                    return render_template('my_note.html',host=HOST,empty='Список пуст, выбирите задачу!')
                else:
                    return render_template('my_note.html',host=HOST,notes=notes,note=int(request.form['note']),method="отменить")
        else:
            return redirect("http://{host}/my-note".format(host=HOST), code=302)

@app.route('/archive',methods=['GET'])
def archive():
    db = Database()
    token = request.cookies.get('session')
    user = db.SelectCheckToken(token)
    if request.method == 'GET':
        if user.Error:
            return redirect("http://{host}/login".format(host=HOST), code=302)
        else:
            notes = db.SelectNotes('archive')
            if notes.Error:
                return render_template('archive.html',host=HOST,empty='Произошла ошибка. Попробуйте обновить страницу!')
            else:
                if len(notes.Data) == 0:
                    return render_template('archive.html',host=HOST,empty='Нет отмененных или завершенных задач')
                else:
                    return render_template('archive.html',host=HOST,notes=notes)

@app.route('/statistics',methods=['GET'])
def statistics():
    db = Database()
    token = request.cookies.get('session')
    user = db.SelectCheckToken(token)
    if user.Error:
        return redirect("http://{host}/login".format(host=HOST), code=302)
    else:
        statistics = db.SelectStatistics()
        if statistics.Error:
            return render_template('statistic.html',host=HOST,error='Произошла ошибка. Попробуйте обновить страницу!')
        else:
            return render_template('statistic.html',host=HOST,statistics=statistics)

    return "Success"

@app.route('/logout',methods=['GET'])
def logout():
    db = Database()
    token = request.cookies.get('session')
    data = db.SelectDeleteToken(token)
    if data.Error:
        return 'Не получилось выйти, повторите попытку позже'
    else:
        return redirect("http://{host}/login".format(host=HOST), code=302)